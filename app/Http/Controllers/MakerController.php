<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMakerRequest;
use App\Maker;
use App\Vehicle;
use Illuminate\Support\Facades\Cache;

class MakerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.basic.once', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $makers = Cache::remember('makers', .25, function() {
            return Maker::all();
        });

        return response()->json(['makers' => $makers], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateMakerRequest $request
     * @return Response
     */
    public function store(CreateMakerRequest $request)
    {
        $values = $request->only(['name', 'phone']);

        Maker::create($values);

        return response()->json(['message' => 'Maker correctly added'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $maker = Maker::find($id);

        if (!$maker) {
            return response()->json(['error' => 'Maker with id ' . $id . ' does not exist', 'code' => 404], 404);
        }

        return response()->json(['maker' => $maker], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateMakerRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(CreateMakerRequest $request, $id)
    {
        $maker = Maker::find($id);

        if (!$maker) {
            return response()->json(['error' => 'Maker with id ' . $id . ' does not exist', 'code' => 404], 404);
        }

        $name  = $request->get('name');
        $phone = $request->get('phone');

        $maker->name  = $name;
        $maker->phone = $phone;

        $maker->save();

        return response()->json(['message' => 'The maker has been updated', 'code' => 200], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $maker = Maker::find($id);

        if(!$maker) {
            return response()->json(['error' => 'Maker with id ' . $id . ' does not exist', 'code' => 404], 404);
        }

        $vehicles = $maker->vehicles;

        $numVehicles = sizeof($vehicles);
        if($numVehicles > 0) {
            return response()->json(['error' => 'This maker ('.$id.') has '.$numVehicles.' associated vehicles', 'code' => 409], 409);
        }

        $maker->delete();
        return response()->json(['message' => 'The maker has been deleted', 'code' => 200], 200);
    }

}
