<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateVehicleRequest;
use App\Maker;
use App\Vehicle;
use Illuminate\Http\Request;

class MakerVehiclesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.basic.once', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return Response
     */
    public function index($id)
    {
        $maker = Maker::find($id);

        if (!$maker) {
            return response()->json(['error' => 'Maker with id ' . $id . ' does not exist', 'code' => 404], 404);
        }

        return response()->json(['vehicles' => $maker->vehicles], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateVehicleRequest $request, $makerId)
    {
        $maker = Maker::find($makerId);

        if (!$maker) {
            return response()->json(['error' => 'Maker does not exist', 'code' => 404], 404);
        }
        $values = $request->all();

        $maker->vehicles()->create($values);

        return response()->json(['message' => 'The vehicle associated was created', 'code' => 201], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $vehicleId)
    {
        $maker = Maker::find($id);

        if (!$maker) {
            return response()->json(['error' => 'Maker with id ' . $id . ' does not exist', 'code' => 404], 404);
        }

        $vehicle = $maker->vehicles->find($vehicleId);

        if (!$vehicle) {
            return response()->json(['error' => 'Vehicle does not exist for this maker', 'code' => 404], 404);
        }

        return response()->json(['vehicle' => $vehicle], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateVehicleRequest $request
     * @param $makerId
     * @param $vehicleId
     * @return Response
     * @internal param int $id
     */
    public function update(CreateVehicleRequest $request, $makerId, $vehicleId)
    {
        // MUST INCLUDE _method=PUT in the fields sent
        $maker = Maker::find($makerId);

        if (!$maker) {
            return response()->json(['error' => 'Maker with id ' . $makerId . ' does not exist', 'code' => 404], 404);
        }

        $vehicle = $maker->vehicles()->find($vehicleId);

        if (!$vehicle) {
            return response()->json(['error' => 'Vehicle does not exist for this maker', 'code' => 404], 404);
        }

        $color    = $request->get('color');
        $power    = $request->get('power');
        $capacity = $request->get('capacity');
        $speed    = $request->get('speed');

        $vehicle->color    = $color;
        $vehicle->power    = $power;
        $vehicle->capacity = $capacity;
        $vehicle->speed    = $speed;

        $vehicle->save();

        return response()->json(['message' => 'The vehicle has been updated', 'code' => 200], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $makerId
     * @param $vehicleId
     * @return Response
     */
    public function destroy($makerId, $vehicleId)
    {
        $maker = Maker::find($makerId);

        if (!$maker) {
            return response()->json(['error' => 'Maker with id ' . $makerId . ' does not exist', 'code' => 404], 404);
        }

        $vehicle = $maker->vehicles()->find($vehicleId);

        if (!$vehicle) {
            return response()->json([
                'error' => 'Vehicle (' . $vehicleId . ') does not exist for this maker',
                'code' => 404
            ], 404);
        }


        $vehicle->delete();

        return response()->json(['message' => 'The vehicle has been deleted', 'code' => 200], 200);
    }

}
